package de.mindcrimeilab.spendb;

import org.junit.Assert;
import org.junit.Test;

import java.util.Calendar;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

/**
 * Test for domain key generator
 */
public class DomainKeyGeneratorTest {
    @Test
    public void generateKey() {
        Calendar cal = Calendar.getInstance();
        cal.set(2016,Calendar.JANUARY,28,17,7,45);
        cal.set(Calendar.MILLISECOND, 123);
        String id = DomainKeyGenerator.generate(cal.getTimeInMillis());
        Assert.assertThat(id, is(equalTo("001R0745123")));
    }

    //@Test
    public void dyn() {
        Assert.assertThat(DomainKeyGenerator.generate(System.currentTimeMillis()), is(equalTo("001R07450123")));
    }
}
