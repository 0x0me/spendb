package de.mindcrimeilab.spendb.donation;

import de.mindcrimeilab.spendb.SpenDB;
import de.mindcrimeilab.spendb.donator.DonatorRepository;
import de.mindcrimeilab.spendb.donator.Person;
import de.mindcrimeilab.spendb.util.TestUtil;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpenDB.class)
@TestPropertySource(locations="classpath:test.properties")
public class HousewaresTest {
    @Autowired
    private DonationRepository donationRepository;

    @Autowired
    private DonatorRepository donatorRepository;

    @Autowired
    private HousewaresItemRepository housewaresItemRepository;

    @Test
    public void testCRUD() {
        Person savedPerson = TestUtil.createPerson(donatorRepository);
        HousewaresItem savedItem = createHousewaresItem();


        Housewares housewares = new Housewares();
        housewares.setName("Plate");
        housewares.setDescription("The famous golden plate");
        housewares.setQuantity(15);
        housewares.setDonator(savedPerson);
        housewares.setHousewaresItemKey(savedItem);

        Housewares savedHousewares = donationRepository.save(housewares);
        check(savedHousewares);

        Long id = savedHousewares.getId();

        AbstractDonation loadedHousewares = donationRepository.findOne(id);
        assertThat(loadedHousewares, Matchers.instanceOf(Housewares.class));
        check((Housewares) loadedHousewares);

    }

    private HousewaresItem createHousewaresItem() {
        HousewaresItem item = new HousewaresItem();
        item.setId(1L);
        item.setKey("DISHES");
        item.setName("Dishes");
        HousewaresItem savedHousewaresItem = housewaresItemRepository.save(item);
        assertNotNull(savedHousewaresItem);
        return savedHousewaresItem;
    }

    private void check(Housewares item) {
        assertNotNull(item);
        assertNotNull(item.getId());
        assertThat(item.getId(), Matchers.greaterThan(0L));

        assertEquals("Plate", item.getName() );
        assertEquals("The famous golden plate", item.getDescription() );
        assertEquals(15, item.getQuantity() );

        HousewaresItem itemType = item.getHousewaresItemKey();
        assertEquals("DISHES", itemType.getKey());
        assertEquals("Dishes", itemType.getName());
    }

}
