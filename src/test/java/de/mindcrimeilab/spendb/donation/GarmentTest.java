package de.mindcrimeilab.spendb.donation;

import de.mindcrimeilab.spendb.SpenDB;
import de.mindcrimeilab.spendb.donator.DonatorRepository;
import de.mindcrimeilab.spendb.donator.Person;
import de.mindcrimeilab.spendb.util.TestUtil;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpenDB.class)
@TestPropertySource(locations="classpath:test.properties")
public class GarmentTest {
    @Autowired
    private DonationRepository donationRepository;

    @Autowired
    private DonatorRepository donatorRepository;

    @Test
    public void testCRUD() {
        Person donator = TestUtil.createPerson(donatorRepository);

        Garment gar = new Garment();
        gar.setGender(Gender.FEMALE);
        gar.setAge(Age.YOUNGSTER);
        gar.setSize("XL");
        gar.setName("T-Shirt");
        gar.setDescription("Cool shirt");
        gar.setQuantity(10);


        Person savedPerson = donatorRepository.save(donator);
        assertNotNull(savedPerson);


        gar.setDonator(savedPerson);

        Garment savedGarment = donationRepository.save(gar);
        check(savedGarment);

        Long id = savedGarment.getId();

        AbstractDonation loadedGarment = donationRepository.findOne(id);
        assertThat(loadedGarment, Matchers.instanceOf(Garment.class));
        check((Garment) loadedGarment);

    }

    private void check(Garment savedGarment) {
        assertNotNull(savedGarment);
        assertNotNull(savedGarment.getId());
        assertThat(savedGarment.getId(), Matchers.greaterThan(0L));

        assertEquals(Gender.FEMALE, savedGarment.getGender());
        assertEquals(Age.YOUNGSTER, savedGarment.getAge());
        assertEquals("XL",savedGarment.getSize() );
        assertEquals("T-Shirt",savedGarment.getName() );
        assertEquals("Cool shirt",savedGarment.getDescription() );
        assertEquals(10,savedGarment.getQuantity() );
    }

}