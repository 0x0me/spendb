package de.mindcrimeilab.spendb.donation;

import de.mindcrimeilab.spendb.SpenDB;
import de.mindcrimeilab.spendb.donator.DonatorRepository;
import de.mindcrimeilab.spendb.donator.Person;
import de.mindcrimeilab.spendb.util.TestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpenDB.class)
@TestPropertySource(locations = "classpath:test.properties")
public class DonationSpecificationsTest {

    @Autowired
    private SearchDonationRepository searchRepository;
    @Autowired
    private DonatorRepository donatorRepository;

    @Autowired
    private DonationRepository donationRepository;

    private static SearchDonation toSearchDonation(Hygiene hygiene) {
        return SearchDonation.builder()
                .age(hygiene.getAge())
                .description(hygiene.getDescription())
                .donator(hygiene.getDonator())
                .gender(hygiene.getGender())
                .id(hygiene.getId())
                .name(hygiene.getName())
                .type(DonationTypes.HYGIENE_NAME)
                .quantity(hygiene.getQuantity())
                .key(hygiene.getKey())
                .build();
    }

    @Before
    public void before() {
        donationRepository.deleteAllInBatch();
        donatorRepository.deleteAll();
    }

    @Test
    public void testDonationMatches() throws Exception {
        Person p = TestUtil.createPerson(donatorRepository);

        Hygiene hygiene = Hygiene.builder().gender(Gender.FEMALE).build();
        hygiene.setName("Deo");
        hygiene.setDescription("Flowers");
        hygiene.setQuantity(1);
        hygiene.setDonator(p);

        AbstractDonation d1 = donationRepository.save(hygiene);

        SearchParams params = SearchParams.builder().gender(Gender.FEMALE).build();
        List<SearchDonation> result = searchRepository.findAll(Specifications.where(DonationSpecifications.donationMatches(params)));

        assertNotNull(result);
        assertEquals(1, result.size());
        assertThat(result, containsInAnyOrder(toSearchDonation((Hygiene) d1)));


        Hygiene hygiene1 = Hygiene.builder().gender(Gender.UNISEX).build();
        hygiene1.setName("Deo2");
        hygiene1.setDescription("uni");
        hygiene1.setQuantity(1);
        hygiene1.setDonator(p);

        AbstractDonation d2 = donationRepository.save(hygiene1);

        params = SearchParams.builder().gender(Gender.FEMALE).build();
        result = searchRepository.findAll(Specifications.where(DonationSpecifications.donationMatches(params)));

        assertNotNull(result);
        assertEquals(2, result.size());
        assertThat(result, containsInAnyOrder(toSearchDonation((Hygiene) d1), toSearchDonation((Hygiene) d2)));

        params = SearchParams.builder().gender(Gender.MALE).build();
        result = searchRepository.findAll(Specifications.where(DonationSpecifications.donationMatches(params)));

        assertNotNull(result);
        assertEquals(1, result.size());
        assertThat(result, containsInAnyOrder(toSearchDonation((Hygiene) d2)));
    }

}