package de.mindcrimeilab.spendb.donation;

import de.mindcrimeilab.spendb.SpenDB;
import de.mindcrimeilab.spendb.donator.DonatorRepository;
import de.mindcrimeilab.spendb.donator.Person;
import de.mindcrimeilab.spendb.util.TestUtil;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpenDB.class)
@TestPropertySource(locations = "classpath:test.properties")
public class HygieneTest {

    @Autowired
    private DonatorRepository donatorRepository;

    @Autowired
    private DonationRepository donationRepository;

    public static Hygiene createHygiene(DonationRepository donationRepository, Person savedPerson) {
        Hygiene hygiene = new Hygiene();
        hygiene.setName("Body Lotion");
        hygiene.setDescription("Body lotion vanilla");
        hygiene.setQuantity(2);
        hygiene.setAge(Age.ADULT);
        hygiene.setGender(Gender.FEMALE);
        hygiene.setDonator(savedPerson);

        return donationRepository.save(hygiene);
    }

    @Test
    public void testCRUD() {
        Person savedPerson = TestUtil.createPerson(donatorRepository);

        Hygiene savedHygiene = createHygiene(donationRepository, savedPerson);
        check(savedHygiene);

        Long id = savedHygiene.getId();

        AbstractDonation loadedHygiene = donationRepository.findOne(id);
        assertThat(loadedHygiene, Matchers.instanceOf(Hygiene.class));
        check((Hygiene) loadedHygiene);

    }

    private void check(Hygiene item) {
        assertNotNull(item);
        assertNotNull(item.getId());
        assertThat(item.getId(), Matchers.greaterThan(0L));

        assertEquals("Body Lotion", item.getName());
        assertEquals("Body lotion vanilla", item.getDescription());
        assertEquals(2, item.getQuantity());
        assertEquals(Age.ADULT, item.getAge());
        assertEquals(Gender.FEMALE, item.getGender());

    }
}
