package de.mindcrimeilab.spendb.util;

import de.mindcrimeilab.spendb.donator.Person;
import org.springframework.data.repository.CrudRepository;

import static org.junit.Assert.assertNotNull;

/**
 * Test helper class
 */
public class TestUtil {


    public static Person createPerson(CrudRepository<Person, Long> donatorRepository) {
        Person donator = Person.builder()
                .firstname("Homer")
                .lastname("Simpson")
                .build();

        Person savedPerson = donatorRepository.save(donator);
        assertNotNull(savedPerson);
        return savedPerson;
    }
}
