package de.mindcrimeilab.spendb.donator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PersonTest {

    /**
     * Small lombok test
     * @throws Exception
     */
    @Test
    public void testSetFirstname() throws Exception {
        final Person p = new Person();
        assertNull(p.getFirstname());
        p.setFirstname("Hello World");
        assertEquals("Hello World", p.getFirstname());
    }
}