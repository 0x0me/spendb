package de.mindcrimeilab.spendb.donator;

import de.mindcrimeilab.spendb.SpenDB;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpenDB.class)
@TestPropertySource(locations = "classpath:test.properties")
public class DonatorRepositoryTest {

    @Autowired
    private DonatorRepository repository;

    @Test
    public void findByFirstnameLikeOrLastnameLikeAllIgnoringCase() throws Exception {
        final String lastname = "Simpson";
        final String firstname = "Lisa";

        final Person homer = Person.builder()
                .firstname(firstname)
                .lastname(lastname)
                .build();


        Person updated = repository.save(homer);

        Long updatedId = updated.getId();
        assertTrue(repository.exists(updatedId));

        assertNotNull(updated);
        assertNotNull(updatedId);
        assertEquals(lastname, updated.getLastname());
        assertEquals(firstname, updated.getFirstname());

        List<Person> result = repository.findByFirstnameLikeOrLastnameLikeAllIgnoringCase("lis%", "lis%");
        assertEquals(1, result.size());

        Person p = result.iterator().next();
        assertEquals(updatedId, p.getId());
        assertEquals(firstname, p.getFirstname());
        assertEquals(lastname, p.getLastname());
    }

    @Test
    public void testCRUD() {
        final String lastname = "Simpson";
        final String firstname = "Homer";
        final String comment = "This is a very important test data comment äößÄÖÜ";

        final String email = "homer@simpson.com";
        final String fax = "+49(1)23/4567890";
        final String mobil = "+49 (1) 23 / 456 78 90";
        final String phone = "123 / 456 78 90";

        final String city = "Springfield / USA";
        final String number = "1";
        final String street = "Evergreen Terrace";
        final String zip = "D-12345";

        final ContactInformation ci = ContactInformation.builder()
                .email(email)
                .fax(fax)
                .mobil(mobil)
                .phone(phone)
                .build();

        final Address address = Address.builder()
                .city(city)
                .number(number)
                .street(street)
                .zip(zip)
                .build();

        final Person homer = Person.builder()
                .lastname(lastname)
                .firstname(firstname)
                .comment(comment)
                .address(address)
                .contactInformation(ci)
                .build();

        /*
         * Insert
         */

        Person saved = repository.save(homer);
        assertNotNull(saved);

        Long savedId = saved.getId();

        assertNotNull(savedId);
        assertEquals(lastname, saved.getLastname());
        assertEquals(firstname, saved.getFirstname());
        assertEquals(comment, saved.getComment());

        ContactInformation savedCI = saved.getContactInformation();
        assertEquals(email, savedCI.getEmail());
        assertEquals(fax, savedCI.getFax());
        assertEquals(mobil, savedCI.getMobil());
        assertEquals(phone, savedCI.getPhone());

        Address savedAddress = saved.getAddress();
        assertEquals(city, savedAddress.getCity());
        assertEquals(number, savedAddress.getNumber());
        assertEquals(street, savedAddress.getStreet());
        assertEquals(zip, savedAddress.getZip());


    }

}
