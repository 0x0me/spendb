package de.mindcrimeilab.spendb.report;

import de.mindcrimeilab.spendb.donation.AbstractDonation;
import de.mindcrimeilab.spendb.donation.DonationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by me on 13.12.15.
 */
@Controller
public class DontationsReportPdfController {

    @Autowired
    private DonationRepository repository;

    @RequestMapping(value = "/generate/pdf.htm", method = RequestMethod.GET)
    public ModelAndView generatePdf(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<AbstractDonation> donations = repository.findAll();

        //ModelAndView modelAndView = new ModelAndView("pdfView", "donations", donations);
        ModelAndView modelAndView = new ModelAndView(new DonationsPdfView(), "donations", donations);

        return modelAndView;
    }
}
