package de.mindcrimeilab.spendb.report;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import de.mindcrimeilab.spendb.donation.AbstractDonation;
import de.mindcrimeilab.spendb.donation.Housewares;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * Gernerates a PDF report of all donations in the list.
 */
public class DonationsPdfView extends AbstractPdfView {
    private static final List<String> COLUMNS = Arrays.asList("Anzahl", "Nummer", "Bezeichnung", "Beschreibung", "Angelegt", "Spender", "Anfordern");

    @Override
    protected Document newDocument() {
        Document document = super.newDocument();

        document.setPageSize(PageSize.A4.rotate());

        return document;
    }

    @Override
    protected void buildPdfMetadata(Map<String, Object> model, Document document, HttpServletRequest request) {
        super.buildPdfMetadata(model, document, request);
        HeaderFooter hfFooter = new HeaderFooter(new Phrase("Seite ", FontFactory.getFont(FontFactory.HELVETICA, 10)), true);
        hfFooter.setAlignment(1);
        document.setFooter(hfFooter);

        HeaderFooter hfHeader = new HeaderFooter(new Phrase(String.format("SpenDB Spendenliste vom %s", new SimpleDateFormat("dd.MM.yyyy HH:mm").format(System.currentTimeMillis())), FontFactory.getFont(FontFactory.HELVETICA, 10)), false);
        hfHeader.setAlignment(1);
        document.setHeader(hfHeader);
    }

    protected void buildPdfDocument(Map model, Document document, PdfWriter writer, HttpServletRequest req, HttpServletResponse resp) throws Exception {
        List<AbstractDonation> donations = (List<AbstractDonation>) model.get("donations");

        PdfPTable table = new PdfPTable(COLUMNS.size());
        table.setWidthPercentage(100.0f);
        table.setWidths(new float[]{ 1.0f, 1.5f, 2.0f, 3.5f, 1.0f, 1.5f, 1.0f});
        table.setSpacingBefore(10);

        // define font for table header row
        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10);
        font.setColor(Color.WHITE);

        // define table header cell
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.GRAY);
        cell.setPadding(5);

        /*
         * write table header
         */
        for(String column : COLUMNS) {
            cell.setPhrase(new Phrase(column, font));
            table.addCell(cell);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        StringBuilder sb = new StringBuilder();
        // write table row data
        for (AbstractDonation donation : donations) {
            table.addCell(String.valueOf(donation.getQuantity()));
            table.addCell(donation.getKey());
            table.addCell(donation.getName());
            //  table.addCell(donation.getType());
            if(!StringUtils.isEmpty(donation.getDescription())) {
                sb.append(donation.getDescription());
            }

            if( donation instanceof Housewares) {
                Housewares housewares = (Housewares) donation;
                if( null != housewares.getHousewaresItemKey()) {
                    if(sb.length() > 0) {
                        sb.append("\r\n");
                    }
                    sb.append(housewares.getHousewaresItemKey().getName());
                }
            }

            if(!StringUtils.isEmpty(donation.getExtraInformation())) {
                if(sb.length() > 0) {
                    sb.append("\r\n");
                }
                sb.append(donation.getExtraInformation());
            }
            table.addCell(sb.toString());
            table.addCell(sdf.format(donation.getCreated()));
            table.addCell(donation.getDonator().getKey());
            table.addCell("");
            sb.delete(0, sb.length());
        }

        document.add(table);

    }
}