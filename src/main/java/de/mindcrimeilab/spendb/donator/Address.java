package de.mindcrimeilab.spendb.donator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Address information of a donator
 */
@Embeddable
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    @Column(name="street", nullable = true)
    private String street;

    @Column(name="number", nullable = true)
    private String number;

    @Column(name="zip", nullable = true)
    private String zip;

    @Column(name="city", nullable = true)
    private String city;

}
