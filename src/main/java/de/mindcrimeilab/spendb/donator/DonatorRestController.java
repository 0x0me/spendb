package de.mindcrimeilab.spendb.donator;

import de.mindcrimeilab.spendb.donation.DonationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

/**
 * Rest Service
 */
@RestController
@RequestMapping("/services/donators")
public class DonatorRestController {
    private final static Logger log = LoggerFactory.getLogger(DonatorRestController.class);

    @Autowired
    private DonatorRepository donatorRepository;

    @Autowired
    private DonationRepository donationRepository;

    @RequestMapping(method = RequestMethod.GET)
    public Page<Person> donators(
            @RequestParam(value = "page", defaultValue = "0", required = false) int page,
            @RequestParam(value="count", defaultValue = "10", required = false) int count,
            @RequestParam(value="order", defaultValue = "ASC", required = false) Sort.Direction direction,
            @RequestParam(value="sort", defaultValue = "id", required = false) String sortProperty

    ) {
        return donatorRepository.findAll(new PageRequest(page, count, new Sort(direction,sortProperty)));
    }

    @RequestMapping(method = RequestMethod.GET, value = "{id}")
    public Person donator(@PathVariable Long id) {
        return donatorRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> save(@RequestBody Person donator) {
        Person result = donatorRepository.save(donator);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(result.getId()).toUri());
        return new ResponseEntity<>(result, httpHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Long id) {
        donationRepository.delete(donationRepository.findByDonatorId(id));
        donatorRepository.delete(id);
        return new ResponseEntity<>(null, null, HttpStatus.ACCEPTED);
    }

    @RequestMapping(method = RequestMethod.GET, params = {"name"})
    public Iterable<Person> search(@RequestParam(value = "name") String name) {
        String wildcardName = "%" + name.replaceAll("%", "") + "%";
        //List<Person> searchResult = donatorRepository.findByFirstnameLikeOrLastnameLikeAllIgnoringCase(wildcardName, wildcardName);
        List<Person> searchResult = donatorRepository.findByFirstnameLikeOrLastnameLikeOrKeyLikeAllIgnoringCase(wildcardName, wildcardName,wildcardName);
        log.trace("Search Result :" + searchResult);
        return searchResult;
    }
}
