package de.mindcrimeilab.spendb.donator;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Data repository definition to support CURD and other database operations on the database layer.
 */
public interface DonatorRepository extends PagingAndSortingRepository<Person, Long> {
    public List<Person> findByFirstnameLikeOrLastnameLikeAllIgnoringCase(String lastname, String firstname);
    public List<Person> findByFirstnameLikeOrLastnameLikeOrKeyLikeAllIgnoringCase(String lastname, String firstname, String key);
}
