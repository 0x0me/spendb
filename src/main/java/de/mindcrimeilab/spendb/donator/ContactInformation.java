package de.mindcrimeilab.spendb.donator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Contact information of a donator
 */
@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContactInformation {
    @Column(name="email", nullable = true)
    private String email;

    @Column(name="phone", nullable = true)
    private String phone;

    @Column(name="mobil", nullable = true)
    private String mobil;

    @Column(name="fax", nullable = true)
    private String fax;
}
