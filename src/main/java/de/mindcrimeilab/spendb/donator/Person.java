package de.mindcrimeilab.spendb.donator;

import de.mindcrimeilab.spendb.DomainKeyGenerator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.UUID;

/**
 * A person is a donator aggregating all information.
 */
@Entity(name = "t_person")
@Table(indexes = {
        @Index(name = "person_lastname_idx", columnList = "lastname", unique = false),
        @Index(name = "person_firstname_idx", columnList = "firstname", unique = false)
})
@Data
@Builder
@AllArgsConstructor
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 2, initialValue = 1, name = "person_id_seq", sequenceName = "person_id_seq")
    private Long id;


    @Column(name= "key", nullable = false, unique = true, insertable = true, updatable = false)
    private String key;

    @Column(name = "lastname", nullable = false)
    private String lastname;

    @Column(name = "firstname", nullable = false)
    private String firstname;

    @Column(name = "comment", length = 4000)
    private String comment;

    @Embedded
    private Address address;

    @Embedded
    private ContactInformation contactInformation;

    public Person() {
        this.address = new Address();
        this.contactInformation = new ContactInformation();
    }

    @PrePersist
    public void prePersistTrigger() {
        if( key == null) {
            this.key = DomainKeyGenerator.INSTANCE.generate("P");
        }
    }

}
