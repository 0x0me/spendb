package de.mindcrimeilab.spendb.donation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/services/housewares/item")
public class HousewaresItemRestController {
    @Autowired
    private HousewaresItemRepository repository;

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<HousewaresItem> housewaresItems() {
        return repository.findAll();
    }
}
