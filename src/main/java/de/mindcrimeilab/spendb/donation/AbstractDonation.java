package de.mindcrimeilab.spendb.donation;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import de.mindcrimeilab.spendb.DomainKeyGenerator;
import de.mindcrimeilab.spendb.donator.Person;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Base class for all donations.
 *
 * This class define the generall persistence properties for all donations.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Furniture.class, name = DonationTypes.FURNITURE_NAME),
        @JsonSubTypes.Type(value = Garment.class, name = DonationTypes.GARMENT_NAME),
        @JsonSubTypes.Type(value = Housewares.class, name = DonationTypes.HOUSEWARES_NAME),
        @JsonSubTypes.Type(value = Hygiene.class, name = DonationTypes.HYGIENE_NAME),
        @JsonSubTypes.Type(value = Other.class, name = DonationTypes.OTHER_NAME),
        @JsonSubTypes.Type(value = Shoes.class, name = DonationTypes.SHOES_NAME),
        @JsonSubTypes.Type(value = Toy.class, name = DonationTypes.TOY_NAME)
})
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@Table(name="t_donation", indexes = {
        @Index(name = "donation_name_idx", columnList = "name", unique = false)
})
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDonation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 2, initialValue = 1, name = "donation_id_seq", sequenceName = "donation_id_seq")
    private Long id;

    @Column(name="name", nullable = false)
    private String name;

    @Column(name="description", nullable = true)
    private String description;

    @Column(name="quantity")
    private int quantity;

    @Column(name="type", insertable = false, updatable = false, nullable = false)
    private String type;

    @OneToOne
    @JoinColumn(name="donator_id", referencedColumnName = "id", nullable = false)
    private Person donator;

    /*
     * values for updateable and insertable are needed to make default value working,
     * setting insertable to false too will result in wrong create date entries on re-import
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created", nullable = false, updatable = false, insertable = true, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;

    @Column(name= "key", nullable = false, unique = true, insertable = true, updatable = false)
    private String key;

    /**
     * Checks and actions to peform before persisting an entry.
     */
    @PrePersist
    public void prePersistTrigger() {
        if( null == created ) {
            created = new Date(System.currentTimeMillis());
        }

        if( key == null) {
            this.key = DomainKeyGenerator.INSTANCE.generate("D");
        }
    }

    public String getExtraInformation() {
        return "";
    }
}
