package de.mindcrimeilab.spendb.donation;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(DonationTypes.SHOES_NAME)
@Data
@EqualsAndHashCode(callSuper = true)
public class Shoes extends AbstractGarment {
}
