package de.mindcrimeilab.spendb.donation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(DonationTypes.OTHER_NAME)
@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Other extends AbstractDonation {
}
