package de.mindcrimeilab.spendb.donation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SearchDonationRepository extends JpaRepository<SearchDonation, Long>, JpaSpecificationExecutor<SearchDonation> {
}
