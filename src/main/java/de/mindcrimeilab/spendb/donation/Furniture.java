package de.mindcrimeilab.spendb.donation;

import lombok.*;
import org.springframework.util.StringUtils;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(DonationTypes.FURNITURE_NAME)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Furniture extends AbstractDonation {
    @Column(name = "size")
    private String size;

    @Override
    public String getExtraInformation() {
        return StringUtils.isEmpty(size) ? "" : String.format("Größe: %s", size);
    }
}
