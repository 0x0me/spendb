package de.mindcrimeilab.spendb.donation;

import lombok.*;
import org.springframework.util.StringUtils;

import javax.persistence.*;

/**
 * Personal hygiene item
 */
@Entity
@DiscriminatorValue(DonationTypes.HYGIENE_NAME)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Hygiene extends AbstractDonation {
    @Column(name = "gender")
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @Column(name = "age")
    @Enumerated(value = EnumType.STRING)
    private Age age;

    @Override
    public String getExtraInformation() {
        StringBuilder sb = new StringBuilder();

        if(!StringUtils.isEmpty(gender) && gender != Gender.UNKNOWN) {
            sb.append(gender.getDisplayName());
        }

        if(!StringUtils.isEmpty(age) && age != Age.UNKNOWN) {
            if( sb.length() > 0) {
                sb.append("; ");
            }
            sb.append(age.getDisplayName());
        }


        return sb.toString();
    }
}
