package de.mindcrimeilab.spendb.donation;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum DonationTypes implements DonationTypeNames {

    GARMENT(DonationTypeNames.GARMENT_NAME),
    SHOES(DonationTypeNames.SHOES_NAME),
    HOUSEWARES(DonationTypeNames.HOUSEWARES_NAME),
    HYGIENE(DonationTypeNames.HYGIENE_NAME),
    TOY(DonationTypeNames.TOY_NAME),
    FURNITURE(DonationTypeNames.FURNITURE_NAME),
    OTHER(DonationTypeNames.OTHER_NAME);

    private final String typeName;

    DonationTypes(String typeName) {
        this.typeName = typeName;
    }

    public static List<String> getDonationTypes() {
        return Arrays.asList(DonationTypes.values())
                .stream()
                .map(DonationTypes::getTypeName)
                .sorted()
                .collect(Collectors.toList());
    }

    public final String getTypeName() {
        return typeName;
    }
}

