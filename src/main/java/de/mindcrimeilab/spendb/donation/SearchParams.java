package de.mindcrimeilab.spendb.donation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Search parameters for donations
 */
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class SearchParams {
    private String name;
    private Age age;
    private Gender gender;
    private DonationTypes type;
    private HousewaresItem housewaresItem;
}
