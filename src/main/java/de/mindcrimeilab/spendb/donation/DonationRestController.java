package de.mindcrimeilab.spendb.donation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

import static de.mindcrimeilab.spendb.donation.DonationSpecifications.donationMatches;

@RestController
@RequestMapping("/services/donations")
public class DonationRestController {
    private final static Logger log = LoggerFactory.getLogger(DonationRestController.class);

    @Autowired
    private DonationRepository donationRepository;

    @Autowired
    private SearchDonationRepository searchDonationRepository;


    @RequestMapping(method = RequestMethod.GET)
    public Page<AbstractDonation> donations(
            @RequestParam(value = "page", defaultValue = "0", required = false) int page,
            @RequestParam(value = "count", defaultValue = "10", required = false) int count,
            @RequestParam(value = "order", defaultValue = "ASC", required = false) Sort.Direction direction,
            @RequestParam(value = "sort", defaultValue = "id", required = false) String sortProperty,
            @RequestParam(value = "type", required = false) DonationTypes type,
            @RequestParam(value = "donatorId", required = false) Long donatorId
    ) {
        final Page<AbstractDonation> p;
        if (null != type || null != donatorId) {
            if (null != type && null == donatorId) {
                p = donationRepository.findByType(new PageRequest(page, count, new Sort(direction, sortProperty)), type.getTypeName());
            } else if (null == type ) {
                p = donationRepository.findByDonatorId(new PageRequest(page, count, new Sort(direction, sortProperty)), donatorId);
            } else {
                p = donationRepository.findByTypeAndDonatorId(new PageRequest(page, count, new Sort(direction, sortProperty)), type.getTypeName(), donatorId);
            }
        } else {
            p = donationRepository.findAll(new PageRequest(page, count, new Sort(direction, sortProperty)));
        }
        return p;
    }

    @RequestMapping(method = RequestMethod.GET, value = "{id}")
    public AbstractDonation donation(@PathVariable Long id) {
        return donationRepository.findOne(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> save(@RequestBody AbstractDonation donation) {
        AbstractDonation result = donationRepository.save(donation);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(result.getId()).toUri());
        return new ResponseEntity<>(result, httpHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Long id) {
        donationRepository.delete(id);
        return new ResponseEntity<>(null, null, HttpStatus.ACCEPTED);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/types")
    public List<String> donationTypes() {
        return DonationTypes.getDonationTypes();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ageValues")
    public List<String> getAgeValues() {
        return Age.getAgeValues();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/genderValues")
    public List<String> getGenderValues() {
        return Gender.getGenderValues();
    }

    @RequestMapping(method = RequestMethod.POST, value = "search")
    public Iterable<SearchDonation> search(@RequestBody SearchParams searchParams) {

        return searchDonationRepository.findAll(Specifications.where(donationMatches(searchParams)));
    }

    @RequestMapping(method = RequestMethod.GET, value = "search")
    public Iterable<SearchDonation> search(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) DonationTypes type,
            @RequestParam(required = false) Age age,
            @RequestParam(required = false) Gender gender,
            @RequestParam(required = false) HousewaresItem housewaresItem
    ) {
        SearchParams searchParams = new SearchParams();
        searchParams.setName(name);
        searchParams.setGender(gender);
        searchParams.setAge(age);
        searchParams.setType(type);
        searchParams.setHousewaresItem(housewaresItem);
        return search(searchParams);
    }
}
