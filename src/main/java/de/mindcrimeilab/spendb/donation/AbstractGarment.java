package de.mindcrimeilab.spendb.donation;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractGarment extends AbstractDonation {
    @Column(name="size")
    private String size;

    @Column(name="gender")
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @Column(name="age")
    @Enumerated(value = EnumType.STRING)
    private Age age;

    @Override
    public String getExtraInformation() {
        StringBuilder sb = new StringBuilder();
        if(!StringUtils.isEmpty(size)) {
            sb.append("Größe: ").append(size);
        }

        if(!StringUtils.isEmpty(gender) && gender != Gender.UNKNOWN) {
            if( sb.length() > 0) {
                sb.append("; ");
            }
            sb.append(gender.getDisplayName());
        }

        if(!StringUtils.isEmpty(age) && age != Age.UNKNOWN) {
            if( sb.length() > 0) {
                sb.append("; ");
            }
            sb.append(age.getDisplayName());
        }


        return sb.toString();
    }
}
