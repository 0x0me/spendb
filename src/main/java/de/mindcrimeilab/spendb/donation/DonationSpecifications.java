package de.mindcrimeilab.spendb.donation;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;

/**
 * Custom search specification for donation search view.
 */
public class DonationSpecifications {
    public static Specification<SearchDonation> donationMatches(final SearchParams params) {
        return (root, query, cb) -> {
            Predicate p = null;
            if (!StringUtils.isEmpty(params.getName())) {
                String pattern = getContainsPattern(params);
                p = cb.or(
                        cb.like(
                                cb.upper(root.get("name")), pattern),
                        cb.like(cb.upper(root.get("description")), pattern)
                );
                p = cb.or(p, cb.like(cb.upper(root.get("key")), pattern));
            }

            if (null != params.getType()) {
                Predicate expr = cb.equal(root.get("type"), params.getType().getTypeName());
                p = (null == p) ? expr : cb.and(p, expr);
            }

            if (null != params.getAge() && params.getAge() != Age.UNKNOWN) {
                Predicate expr = cb.equal(root.get("age"), params.getAge());
                p = (null == p) ? expr : cb.and(p, expr);
            }

            if (null != params.getGender() && params.getGender() != Gender.UNKNOWN) {
                Predicate expr = cb.equal(root.get("gender"), params.getGender());

                // if searching for a certain gender find all unisex items too
                if (params.getGender() != Gender.UNISEX) {
                    expr = cb.or(expr, cb.equal(root.get("gender"), Gender.UNISEX));
                }

                p = (null == p) ? expr : cb.and(p, expr);
            }


            if (null != params.getHousewaresItem() ) {
                Predicate expr = cb.equal(root.get("housewaresItemKey"), params.getHousewaresItem());
                p = (null == p) ? expr : cb.and(p, expr);
            }


            return p;
        };
    }

    public static Specification<SearchDonation> hasAgeGenger(final SearchParams params) {
        return (root, query, cb) -> {
            Predicate p = null;

            if (params.getAge() != Age.UNKNOWN) {
                Predicate expr = cb.equal(root.get("age"), params.getAge());
                p = expr;
            }

            if (params.getGender() != Gender.UNKNOWN) {
                Predicate expr = cb.equal(root.get("gender"), params.getGender());
                p = (null == p) ? expr : cb.and(p, expr);
            }
            return p;
        };
    }

    private static String getContainsPattern(SearchParams params) {
        return ( params == null || StringUtils.isEmpty(params)) ? "%" : "%" + params.getName().toUpperCase() + "%";
    }
}
