package de.mindcrimeilab.spendb.donation;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * Housewares.
 */
@Entity
@DiscriminatorValue(DonationTypes.HOUSEWARES_NAME)
@Data
@EqualsAndHashCode(callSuper = true)
public class Housewares extends AbstractDonation {
    @OneToOne
    @JoinColumn(name="housewaresitem_key", referencedColumnName = "key")
    private HousewaresItem housewaresItemKey;
}
