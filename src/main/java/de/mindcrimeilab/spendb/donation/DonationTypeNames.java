package de.mindcrimeilab.spendb.donation;

public interface DonationTypeNames {
    String GARMENT_NAME = "GARMENT";
    String SHOES_NAME = "SHOES";
    String HOUSEWARES_NAME = "HOUSEWARES";
    String HYGIENE_NAME = "HYGIENE";
    String TOY_NAME = "TOY";
    String FURNITURE_NAME = "FURNITURE";
    String OTHER_NAME = "OTHER";
}
