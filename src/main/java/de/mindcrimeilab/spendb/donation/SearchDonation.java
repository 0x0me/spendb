package de.mindcrimeilab.spendb.donation;

import de.mindcrimeilab.spendb.donator.Person;
import lombok.*;

import javax.persistence.*;


@Entity
@Table(name="t_donation", indexes = {
        @Index(name = "donation_name_idx", columnList = "name", unique = false)
})
@Getter
@ToString
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchDonation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 2, initialValue = 1, name = "donation_id_seq", sequenceName = "donation_id_seq")
    private Long id;

    @Column(name="name", nullable = false, updatable = false)
    private String name;

    @Column(name="description", nullable = true, updatable =  false)
    private String description;

    @Column(name="quantity")
    private int quantity;

    @Column(name="type", insertable = false, updatable = false, nullable = false)
    private String type;

    @OneToOne
    @JoinColumn(name="donator_id", referencedColumnName = "id", nullable = false, updatable =  false)
    private Person donator;

    @Column(name="size", nullable = true, updatable =  false)
    private String size;

    @Column(name="gender", nullable = true, updatable =  false)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @Column(name="age", nullable = true, updatable =  false)
    @Enumerated(value = EnumType.STRING)
    private Age age;

    @OneToOne
    @JoinColumn(name="housewaresitem_key", referencedColumnName = "key", nullable = false, updatable = false)
    private HousewaresItem housewaresItemKey;

    @Column(name= "key", nullable = false, unique = true, insertable = false, updatable = false)
    private String key;
}
