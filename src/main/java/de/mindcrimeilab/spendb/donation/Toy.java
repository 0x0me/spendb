package de.mindcrimeilab.spendb.donation;


import lombok.*;
import org.springframework.util.StringUtils;

import javax.persistence.*;

@Entity
@DiscriminatorValue(DonationTypes.TOY_NAME)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Toy extends AbstractDonation {
    @Column(name = "age")
    @Enumerated(value = EnumType.STRING)
    private Age age;

    @Override
    public String getExtraInformation() {
       return (!StringUtils.isEmpty(age) && age != Age.UNKNOWN) ? age.getDisplayName() : "";
    }
}
