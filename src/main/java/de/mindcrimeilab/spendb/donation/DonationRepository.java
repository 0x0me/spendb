package de.mindcrimeilab.spendb.donation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DonationRepository extends JpaRepository<AbstractDonation, Long> {
    Page<AbstractDonation> findByType(Pageable p, String type);

    Page<AbstractDonation> findByTypeAndDonatorId(Pageable p, String type, Long donatorId);

    Page<AbstractDonation> findByDonatorId(Pageable p, Long donatorId);

    Iterable<AbstractDonation> findByDonatorId(Long donatorId);
}
