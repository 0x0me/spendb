package de.mindcrimeilab.spendb.donation;


import org.springframework.data.repository.CrudRepository;

public interface HousewaresItemRepository extends CrudRepository<HousewaresItem, Long> {
}
