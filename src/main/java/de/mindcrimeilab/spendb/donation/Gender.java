package de.mindcrimeilab.spendb.donation;


import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum Gender {
    MALE("männlich"), FEMALE("weiblich"), UNISEX("unisex"), UNKNOWN("unbekannt");

    public static List<String> getGenderValues() {
        return Arrays.asList(Gender.values()).stream().map( x -> x.name() ).sorted().collect(Collectors.toList());
    }

    @Getter
    private String displayName;


    Gender(String displayName) {
        this.displayName = displayName;
    }

}
