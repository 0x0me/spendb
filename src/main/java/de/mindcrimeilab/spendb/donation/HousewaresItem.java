package de.mindcrimeilab.spendb.donation;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Class represents a certain - catalog like - type of an housewares item.
 */
@Entity
@Data
@Table(name = "t_housewaresitem", indexes = {
        @Index(name = "housewaresitem_name_idx", columnList = "key", unique = true)
})
public class HousewaresItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 2, initialValue = 1, name = "housewaresitem_id_seq", sequenceName = "housewaresitem_id_seq")
    private Long id;

    @Column(name="key", nullable = false, unique = true)
    private String key;

    @Column(name="name")
    private String name;

}
