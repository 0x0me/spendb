package de.mindcrimeilab.spendb.donation;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by me on 22.09.15.
 */
public enum Age {
    BABY("Baby"),
    CHILD("Kind"),
    YOUNGSTER("Jungendliche(r)"),
    ADULT("Erwachsene(r)"),
    UNKNOWN("unbekannt");

    public static List<String> getAgeValues() {
        return Arrays.asList(Age.values()).stream().map(x -> x.name() ).sorted().collect(Collectors.toList());
    }

    @Getter
    private String displayName;

    Age(String displayName) {
        this.displayName = displayName;
    }
}
