package de.mindcrimeilab.spendb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;


public enum DomainKeyGenerator {
    INSTANCE;

    private final static Logger log = LoggerFactory.getLogger(DomainKeyGenerator.class);

    private final static int YEAR_BASE_OFFSET = 2016;

    private final static String NUM_TO_CHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    DomainKeyGenerator() {}

    public final synchronized String generate(String prefix) {
        // for safety sleep 1ms to guarantee unique keys
        try {
            Thread.currentThread().sleep(1);
        } catch (InterruptedException e) {
            //ignore
        }
        String keyId = DomainKeyGenerator.generate(System.currentTimeMillis());
        return String.format("%s%s", prefix, keyId);
    }


    static final String generate(long timestamp) {
        StringBuilder sb = new StringBuilder();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);

        int year = cal.get(Calendar.YEAR);
        int modYear = year % YEAR_BASE_OFFSET;

        int month = cal.get(Calendar.MONTH);
        String hexMonth = String.format("%01X",month);

        int day = cal.get(Calendar.DAY_OF_MONTH)-1;
        char dayChar = numToChar(day);

        int hour = cal.get(Calendar.HOUR_OF_DAY);
        char hourChar = numToChar(hour);

        String minute = String.format("%02d", cal.get(Calendar.MINUTE));
        String second = String.format("%02d", cal.get(Calendar.SECOND));
        String millis = String.format("%03d", cal.get(Calendar.MILLISECOND));

        sb.append(modYear);
        sb.append(hexMonth);
        sb.append(dayChar);
        sb.append(hourChar);
        sb.append(minute);
        sb.append(second);
        sb.append(millis);

        return sb.toString();
    }

    private static char numToChar(int value) {
        if( value < 0 || value > 35 ) throw new IllegalArgumentException("Value should be between 0 and 35");
        return NUM_TO_CHAR.charAt(value);
    }
}
