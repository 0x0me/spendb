package de.mindcrimeilab.spendb.config;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * SpenDB Spring / JPA database configuration.
 */
@EnableJpaRepositories(basePackages = {"de.mindcrimeilab.spendb.donator"})
public class DatabaseConfiguration {
}
