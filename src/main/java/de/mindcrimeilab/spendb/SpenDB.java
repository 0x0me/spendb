package de.mindcrimeilab.spendb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpenDB main class
 */
@SpringBootApplication
@EnableAutoConfiguration
public class SpenDB {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(SpenDB.class, args);
    }
}
