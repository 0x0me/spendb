package de.mindcrimeilab.spendb.transfer;

import de.mindcrimeilab.spendb.donation.DonationRepository;
import de.mindcrimeilab.spendb.donation.HousewaresItemRepository;
import de.mindcrimeilab.spendb.donator.DonatorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/services/export")
public class ExportRestController {
    private final static Logger log = LoggerFactory.getLogger(ExportRestController.class);

    @Autowired
    private DonatorRepository donatorRepository;

    @Autowired
    private DonationRepository donationRepository;

    @Autowired
    private HousewaresItemRepository housewaresItemRepository;


    @RequestMapping(method = RequestMethod.GET)
    public
    @ResponseBody
    Backup exportAll() {
        Backup.BackupBuilder builder = Backup.builder();
        housewaresItemRepository.findAll().forEach(it -> builder.housewaresItem(it));
        donatorRepository.findAll().forEach(it -> builder.donator(it));
        donationRepository.findAll().forEach(it -> builder.donation(it));
        return builder.build();
    }


}
