package de.mindcrimeilab.spendb.transfer;

import de.mindcrimeilab.spendb.donation.AbstractDonation;
import de.mindcrimeilab.spendb.donation.HousewaresItem;
import de.mindcrimeilab.spendb.donator.Person;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Backup {
    private
    @Singular
    List<HousewaresItem> housewaresItems;
    private
    @Singular
    List<Person> donators;
    private
    @Singular
    List<AbstractDonation> donations;

}
