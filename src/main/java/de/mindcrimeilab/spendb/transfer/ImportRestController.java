package de.mindcrimeilab.spendb.transfer;

import de.mindcrimeilab.spendb.donation.DonationRepository;
import de.mindcrimeilab.spendb.donation.HousewaresItemRepository;
import de.mindcrimeilab.spendb.donator.DonatorRepository;
import de.mindcrimeilab.spendb.donator.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/services/import")
public class ImportRestController {
    private final static Logger log = LoggerFactory.getLogger(ImportRestController.class);

    @Autowired
    private DonatorRepository donatorRepository;

    @Autowired
    private DonationRepository donationRepository;

    @Autowired
    private HousewaresItemRepository housewaresItemRepository;


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> importAll(@RequestBody Backup backup) {
        // first import all master data
        backup.getHousewaresItems().forEach(it -> housewaresItemRepository.save(it));

        /* During import we recreating the whole data base, to keep a consistent state
         * save origId and imported person in order to replace the old id from import file
         * with the correct version saved in the database
         */
        Map<Long, Person> donatorIds = new HashMap<>();

        /*
         * import donators
         */

        long donatorsCount = backup.getDonators()
                .stream()
                .map(it -> {
                    Long oldId = it.getId();
                    it.setId(null);
                    Person saved = donatorRepository.save(it);
                    donatorIds.put(oldId, saved);
                    return it;
                })
                .count();

        long donationsCount = backup.getDonations()
                .stream()
                .map(it -> {
                    Long oldDonatorId = it.getDonator().getId();
                    Person donator = donatorIds.get(oldDonatorId);
                    it.setDonator(donator);
                    return it;
                })
                .map(it -> donationRepository.save(it))
                .count();


        Map<String, Long> result = new HashMap<>();
        result.put("donators", Long.valueOf(donatorsCount));
        result.put("donations", Long.valueOf(donationsCount));

        return new ResponseEntity<>(result, HttpStatus.ACCEPTED);
    }

}
