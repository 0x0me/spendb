'use strict';

var app = angular.module('spenDB', [
    'ngRoute',
    'ngResource',
    'spenDB.about',
    'spenDB.donators',
    'spenDB.donations',
    'spenDB.backup'
]);

/*
 * Configure production mode
 *
 * If you wish to debug an application with this information then you should
 * open up a debug console in the browser then call this method directly in
 * this console:
 * <code>angular.reloadWithDebugInfo();</code>
 */
app.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
}]);

/*
 * configure global routing
 */
var routeConfig = app.config(['$routeProvider', function ($routeProvider) {
    return $routeProvider.otherwise({
        redirectTo: '/about'
    });
}]);

/*
 * Menu selection logic
 */
routeConfig.run(['$rootScope', '$location', '$log', function ($rootScope, $location, $log) {
    $rootScope.$on("$routeChangeStart", function (event, next /*, current */) {
        $rootScope.selectedMenu = next.originalPath;
        $log.debug('selected menu is [' + $rootScope.selectedMenu + ']');
    });
}]);

/*
 * trace history
 */
app.run(['$rootScope', '$location', '$log', function ($rootScope, $location, $log) {
    var history = [];

    // save old path on stack on route changes
    $rootScope.$on('$routeChangeSuccess', function () {
        var oldPath = history.length > 1 ? history[history.length - 1] : '/';
        var newPath = $location.$$path;
        $log.debug('Pushing to history [' + newPath + ']');
        $log.debug('Old path is [' + oldPath + ']');
        if (oldPath.replace(newPath, '').length > 0) {
            history.push(newPath);
        }
    });

    /**
     * Function to navigate back in the path by poping one element from
     * history.
     */
    $rootScope.back = function () {
        var prevUrl = history.length > 1 ? history.splice(-2)[0] : '/';
        $log.debug('Going back to [' + prevUrl + ']');
        $location.path(prevUrl);
    };
}]);

/*
 * Preload version
 */
app.run(['$rootScope', '$resource', function ($rootScope, $resource) {
    $rootScope.version = $resource('version.json').get();
}]);

app.filter('localize', function () {
    return function (input) {
        var output = '';
        switch (input) {
            case 'ADULT' :
                output = 'Erwachsener';
                break;
            case 'BABY' :
                output = 'Baby / Kleinkind';
                break;
            case 'CHILD' :
                output = 'Kind';
                break;
            case 'YOUNGSTER' :
                output = 'Jugendlicher';
                break;
            case 'FEMALE' :
                output = 'weiblich';
                break;
            case 'MALE' :
                output = 'männlich';
                break;
            case 'UNKNOWN' :
                output = 'unbekannt';
                break;
            case 'UNISEX' :
                output = 'unisex';
                break;
            case 'GARMENT' :
                output = 'Kleidung';
                break;
            case 'SHOES' :
                output = 'Schuhe';
                break;
            case 'HOUSEWARES' :
                output = 'Haushaltswaren';
                break;
            case 'HYGIENE' :
                output = 'Körperpflegeartikel';
                break;
            case 'TOY' :
                output = 'Spielzeug';
                break;
            case 'FURNITURE' :
                output = 'Möbel';
                break;
            case 'OTHER' :
                output = 'Sonstiges';
                break;
            default:
                output = input;
        }
        return output;
    }
});

app.filter('asRangeArray',['$log', function ($log) {
    return function (input, totalStr, startIndexStr, countStr) {
        var total = parseInt(totalStr);
        var startIndex = parseInt(startIndexStr);
        var count = parseInt(countStr);
        for(var i = 0; i < total; i++) {
            input.push(i);
        }
        if( Number.isInteger(startIndex) && Number.isInteger(count)) {
            if( startIndex + count > total ) {
                startIndex = total - count;
            }
            if( startIndex < 0 ) {
                startIndex = 0;
            }
            return input.slice(startIndex, startIndex+count);
        }
        else {
            return input;
        }
    };
}]);
