'use strict';

var app = angular.module('spenDB');

/**
 * Install global fault and success message handlers
 */
app.run(['$rootScope', '$log', function ($rootScope, $log) {
    /**
     * Handle errors
     * @param errorData
     */
    $rootScope.faultHandler = function (errorData) {
        $rootScope.errorMessage = {
            msg: buildErrorMessage(errorData),
            ttl: 1
        };
    };

    /**
     * Handle success messages
     * @param successData
     */
    $rootScope.successHandler = function (successData) {
        $rootScope.successMessage = {
            msg: successData,
            ttl: 1
        };
    };

    /**
     * Clear error messages
     */
    $rootScope.clearFault = function () {
        $log.debug('[faultHandler] clearing fault[' + $rootScope.errorMessage + ']');
        $rootScope.errorMessage = handleTtl($rootScope.errorMessage);
    };

    /**
     * Clear success messages
     */
    $rootScope.clearSuccess = function () {
        $log.debug('[faultHandler] clearing fault[' + $rootScope.successMessage + ']');
        $rootScope.successMessage = handleTtl($rootScope.successMessage);
    };

    /**
     * Decrement ttl (time-to-live) counter of ttlMessage
     * @param ttlMessage
     * @returns {*}
     */
    function handleTtl(ttlMessage) {
        if( typeof(ttlMessage) !== 'undefined') {
            var ttl =  ttlMessage.ttl;
            if( ttl > 0) {
                ttlMessage.ttl = ttl - 1;
            }
            else {
                ttlMessage = undefined;
            }
        }
        return ttlMessage;
    }
}]);

/**
 * Directive for displaying messages
 * @type {*|ng.$compileProvider}
 */
var directive = app.directive('errorMessages', function () {
        return {
            template: '<div class="alert alert-danger" data-ng-show="errorMessage"><strong>Achtung:&nbsp;</strong><span data-ng-bind="errorMessage.msg"></span></div>' +
            '<div class="alert alert-success" data-ng-show="successMessage"><span data-ng-bind="successMessage.msg"></span></div>',
            restrict: 'E',
            controller: 'ErrorMessagesCtrl'

        }
    }
);

/**
 * Controller taking care of displaying a message once and removing them afterwards
 */
directive.controller('ErrorMessagesCtrl', ['$scope', '$rootScope', '$log', function ($scope, $rootScope, $log) {
    $scope.$on('$viewContentLoaded', function () {
        $log.debug('[ErrorMessagesCtrl]: running ctrl');
        $scope.successMessage = angular.copy($rootScope.successMessage);
        $scope.errorMessage = angular.copy($rootScope.errorMessage);
        $log.debug('[ErrorMessagesCtrl]: successMessage=[' + $scope.successMessage + ']');
        $log.debug('[ErrorMessagesCtrl]: errorMessage=[' + $scope.errorMessage + ']');
        $rootScope.clearFault();
        $rootScope.clearSuccess();
        $log.debug('[ErrorMessagesCtrl]: all messages cleared');
    });
}]);

/**
 * Tries to extract some kind of useful message form errData
 * @param errData
 */
function buildErrorMessage(errData) {
    console.log(errData);
    var message = errData;
    if (typeof(errData['data']) !== 'undefined' && errData['data'] !== null) {
        message = ''+errData.data.status+' '+errData.data.error;
        if (typeof(errData.data['message'] === 'string')) {
            message = message +'. '+ errData.data.message;
        }
    }
    return message;
}