'use strict';

var donations = angular.module('spenDB.donations', [
    'ngRoute'
]);

donations.config(['$routeProvider', function ($routeProvider) {
    return $routeProvider
        .when('/add-donation', {
            templateUrl: 'components/donations/edit.html',
            controller: 'EditDonationController'
        })
        .when('/edit-donation/:id', {
            templateUrl: 'components/donations/edit.html',
            controller: 'EditDonationController'
        })
        .when('/list-donations', {
            templateUrl: 'components/donations/list.html',
            controller: 'ListDonationController'
        })
        .when('/list-donations/donator/:id', {
            templateUrl: 'components/donations/list.html',
            controller: 'ListDonationController'
        })
        .when('/delete-donation/:id', {
            templateUrl: 'components/donations/confirmDelete.html',
            controller: 'DeleteDonationController'
        })
        .when('/search-donations', {
            templateUrl: 'components/donations/search.html',
            controller: 'SearchDonationController'
        })
}]);

donations.service('HousewaresItemService', ['$resource', function ($resource) {
    return $resource('/services/housewares/item/:id', {}, {
        itemTypes: {
            method: 'GET',
            isArray: true
        }
    });
}]);


donations.service('DonationService', ['$resource', function ($resource) {
    return $resource('/services/donations/:id', {}, {
        query: {
            method: 'GET',
            isArray: false
        },
        search: {
            url: '/services/donations/search',
            method: 'GET',
            isArray: true
        },
        ageValues: {
            method: 'GET',
            params: {
                id: 'ageValues'
            },
            isArray: true
        },
        genderValues: {
            method: 'GET',
            params: {
                id: 'genderValues'
            },
            isArray: true
        },
        donationTypes: {
            method: 'GET',
            params: {
                id: 'types'
            },
            isArray: true
        }
    });
}]);


donations.directive('donationList', function () {
    return {
        templateUrl: 'components/donations/partials/donationList.html',
        restrict: 'E',
        scope: {
            donations: '=',
            query: '=',
            mode: '@'
        },
        controller: 'DonationListController'
    }
});

donations.directive('donationDetail', function () {
    return {
        templateUrl: 'static/components/donations/partials/donationDetail.html',
        restrict: 'E',
        scope: {
            donation: '=donation',
            select: '=select'
        }
    }
});

donatorList.controller('DonationListController', ['$scope', '$log', '$route', function ($scope, $log, $route) {
    $scope.route = $route;

    /**
     *
     * @param msgId
     */
    $scope.showDetail = function (msgId) {
        $log.debug('[Directive] clicked');
        $scope.$parent.detail(msgId);
    }
}]);


donations.controller('EditDonationController', ['$scope', '$rootScope', '$log', '$routeParams', 'DonationService', 'DonatorService', 'HousewaresItemService', function ($scope, $rootScope, $log, $routeParams, DonationsService, DonatorService, HousewaresItemService) {
    $scope.success = false;

    $scope.donation = {
        quantity: 1,
        type: $routeParams['type']
    };

    $scope.ageValues = DonationsService.ageValues();
    $scope.genderValues = DonationsService.genderValues();
    $scope.itemTypes = HousewaresItemService.itemTypes();

    var id = $routeParams['id'];
    $log.debug('Editing donation [' + id + ']');

    if (typeof(id) === 'undefined') {
        /*
         * Use case new donation. Initialize donator if possible
         */
        var donatorId = $routeParams['donatorId'];
        $log.debug('Donator is [' + donatorId + ']');

        if (typeof(donatorId) !== 'undefined') {
            DonatorService.get({id: donatorId}).$promise
                .then(function (data) {
                    $scope.donation.donator = data;
                })
                .catch($rootScope.faultHandler);
        }
    }
    else {
        /*
         * Use case edit donation. Load existing donation
         */
        DonationsService.get({id: id}).$promise
            .then(function (data) {
                $scope.donation = data;
            })
            .catch($rootScope.faultHandler);
    }

    /**
     *  Save new or updated donation to database
     */
    $scope.save = function () {
        $log.debug('Adding new garment');
        DonationsService.save($scope.donation).$promise
            .then(function (data) {
                $log.debug('Successful ' + data);
                $rootScope.successHandler('Spende erfolgreich gespeichert [' + data.id + '].');
                $rootScope.back();
            })
            .catch($rootScope.faultHandler);
    };

    /**
     *
     * @returns {boolean}
     */
    $scope.hasDonator = function () {
        return typeof($scope.donation.donator) !== 'undefined';
    };
}]);

donations.controller('ListDonationController', ['$scope', '$rootScope', '$log', '$route', '$routeParams', '$location', 'DonationService', 'DonatorService', function ($scope, $rootScope, $log, $route, $routeParams, $location, DonationService, DonatorService) {
    $scope.route = $route;

    $scope.maxCount = 20;

    $scope.donations = [];

    $scope.donationTypes = DonationService.donationTypes();

    $scope.donatorId = $routeParams['id'];

    $scope.showPage = function (page) {
        DonationService.query({
            page: (page < 0) ? 0 : page,
            count: 15,
            sort: 'name',
            order: 'ASC',
            type: $scope.type,
            donatorId: $scope.donatorId
        }).$promise
            .then(function (data) {
                $scope.donations = data.content;
                $scope.totalPages = data.totalPages;
                $scope.currentPage = data.number;
                $scope.isFirst = data.first;
                $scope.isLast = data.last;

                if (typeof($scope.donatorId) !== 'undefined') {
                    $scope.donator = DonatorService.get({id: $scope.donatorId});
                }
                else {
                    $scope.donator = undefined;
                }
            })
            .catch($rootScope.faultHandler);
    };

    $scope.showPage(0);

    /**
     *
     * @param id
     */
    $scope.detail = function (id) {
        $scope.selectedDonation = DonationService.get({id: id});
        $('#detailDonationDialog').modal();
    };

    $scope.filter = function () {
        $log.debug('Apply filter for [' + $scope.currentPage + ']');
        $scope.showPage($scope.currentPage);
    };

    /**
     * Removes donation spends by donator filter
     */
    $scope.removeDonatorFilter = function () {
        $log.debug('Removing donator filter');
        $scope.donatorId = null;
        $scope.donator = undefined;
        $location.path('/list-donations');
    }
}]);

donators.controller('DeleteDonationController', ['$scope', '$rootScope', '$routeParams', '$log', 'DonationService', function ($scope, $rootScope, $routeParams, $log, DonationService) {
    $log.debug('Deleting');
    $scope.success = false;
    $scope.id = $routeParams['id'];

    /**
     *
     * @param id
     */
    $scope.delete = function (id) {
        $log.debug('Deleting donation [' + id + ']');
        DonationService.remove({id: id}).$promise
            .then(function (data) {
                $log.debug('Successful ' + data);
                $scope.success = true;
                $rootScope.successHandler('Spende wurde gelöscht.');
                $rootScope.back();
            })
            .catch($rootScope.faultHandler);
    };
}]);

donations.controller('SearchDonationController', ['$scope', '$rootScope', '$log', '$routeParams', '$route', 'DonationService', 'HousewaresItemService', function ($scope, $rootScope, $log, $routeParams, $route, DonationService, HousewaresItemService) {
    // pre-fetch catalog values
    $scope.ageValues = DonationService.ageValues();
    // pre-fetch catalog values
    $scope.genderValues = DonationService.genderValues();

    // pre-fetch catalog values
    $scope.donationTypes = DonationService.donationTypes();

    $scope.itemTypes = HousewaresItemService.itemTypes();

    $scope.searchParams = {};
    $scope.route = $route;

    /**
     *
     */
    $scope.search = function () {
        DonationService.search({
            name: $scope.searchParams.name,
            age: $scope.searchParams.age,
            gender: $scope.searchParams.gender,
            type: $scope.searchParams.type,
            housewaresItemKey: $scope.searchParams.housewaresItemKey
        }).$promise
            .then(function (data) {
                $log.debug('search returned [' + data + ']');
                $scope.donations = data;
            })
            .catch($rootScope.faultHandler);
    };

    /**
     *
     * @param id
     */
    $scope.detail = function (id) {
        $log.debug('[Directive] clicked');
        $scope.selectedDonation = DonationService.get({id: id});
        $('#detailDonationDialog').modal();
    }
}]);