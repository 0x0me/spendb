'use strict';

var app = angular.module('spenDB');

app.directive('dropload', ['$log', function ($log) {
    $log.debug("init dropload");
    return {
        template: '<div class="dropload" ></div>',
        restrict: 'E',
        replace: true,
        link: function (scope, element /*, attrs */) {
            element.bind('dragover', function () {
                element.className = 'dropload hover';
                return false;
            });


            element.bind('dragend', function () {
                element.className = 'dropload';
                return false;
            });

            element.bind('drop', function (evt) {
                element.className = 'dropload';
                evt.stopPropagation();
                evt.preventDefault();

                var file = evt.originalEvent.dataTransfer.files[0];
                var reader = new FileReader();
                reader.onload = function (event) {
                    $log.debug('Event onload [' + event.target + ']');
                    scope.load(event.target.result);
                    scope.$apply(function () {
                        scope.status = '' + file.name + ' loaded.';
                    });
                };

                $log.debug('Reading file [' + file + ']');
                //reader.readAsDataURL(file);
                reader.readAsText(file);
                return false;
            });
        }
    }
}]);