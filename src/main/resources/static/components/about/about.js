'use strict';

var about = angular.module('spenDB.about', [
  'ngRoute'
]);

about.config(['$routeProvider', function ($routeProvider) {
  return $routeProvider.when('/about', {
    templateUrl: 'components/about/about.html'
  });
}]);

