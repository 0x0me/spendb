'use strict';

var donators = angular.module('spenDB.donators', [
    'ngRoute'
]);

donators.config(['$routeProvider', function ($routeProvider) {
    return $routeProvider
        .when('/donators', {
            templateUrl: 'components/donators/search.html',
            controller: 'SearchDonatorCtrl'
        })
        .when('/add-donator', {
            templateUrl: 'components/donators/edit.html',
            controller: 'EditDonatorCtrl'
        })
        .when('/edit-donator/:id', {
            templateUrl: 'components/donators/edit.html',
            controller: 'EditDonatorCtrl'
        })
        .when('/delete-donator/:id', {
            templateUrl: 'components/donators/confirmDelete.html',
            controller: 'DeleteDonatorCtrl'
        })
        .when('/list-donators', {
            templateUrl: 'components/donators/list.html',
            controller: 'ListDonatorsCtrl'
        });

}]);

donators.service('DonatorService', ['$resource', function ($resource) {
    return $resource('/services/donators/:id', {}, {
        query: {
            method: 'GET',
            isArray: false
        },
        search: {
            method: 'GET',
            isArray: true
        }
    });
}]);

var donatorList = donators.directive('donatorList', function () {
    return {
        templateUrl: 'components/donators/partials/donatorsTable.html',
        restrict: 'E',
        scope: {
            donators: '=',
            query: '=',
            donatorId: '=donatorid',
            mode: '@'
        },
        controller: 'DonatorListController'
    }
});

donatorList.controller('DonatorListController', ['$scope', '$log', '$route', function ($scope, $log, $route) {
    $scope.route = $route;
    $scope.showDetail = function (msgId) {
        $log.debug('[Directive] clicked');
        $scope.$parent.detail(msgId);
    }
}]);

var detailDirective = donators.directive('donatorDetail', function () {
    return {
        templateUrl: 'static/components/donators/partials/donatorDetail.html',
        restrict: 'E',
        scope: {
            donator: '=donator',
            select: '=select'
        },
        controller: 'DonatorDetailController'
    }
});

detailDirective.controller('DonatorDetailController', ['$rootScope', '$scope', function ($rootScope, $scope) {
    $scope.edit = function (id) {
        $scope.$parent.edit(id);
    };

    $scope.delete = function (id) {
        $scope.$parent.delete(id);
    };

    $scope.showDetail = function (id) {
        $scope.$parent.showDetail(id);
    };
}]);

donators.controller('SearchDonatorCtrl', ['$scope', '$rootScope', '$log', 'DonatorService', function ($scope, $rootScope, $log, DonatorService) {
    $scope.donators = undefined;
    $scope.searchInProgress = false;

    $scope.search = function () {
        $scope.searchInProgress = true;
        search($scope.name);
    };

    function search(name) {
        $log.debug('Searching... with [' + name + ']');
        DonatorService.search({name: name}).$promise
            .then(function (data) {
                $log.debug('Got search result [' + data + ']');
                $scope.donators = data;
            })
            .catch($rootScope.errorHandler)
            .finally(function () {
                $scope.searchInProgress = false;
            });
    }
}]);

donators.controller('EditDonatorCtrl', ['$scope', '$rootScope', '$routeParams', '$log', 'DonatorService', function ($scope, $rootScope, $routeParams, $log, DonatorService) {
    $scope.success = false;
    var id = $routeParams['id'];
    $log.debug('Editing donator [' + id + ']');

    if (typeof(id) !== 'undefined') {
        DonatorService.get({id: id}).$promise
            .then(function (data) {
                $log.debug('Edit' + data);
                $scope.donator = data;
            })
            .catch(function (err) {
                $rootScope.faultHandler(err);
            });
    }
    /**
     *  Update donator in database
     */
    $scope.save = function () {
        $log.debug('Updating donator');

        DonatorService.save($scope.donator).$promise
            .then(function (data) {
                $log.debug('Successful ' + data);
                $scope.success = true;
                $rootScope.successHandler('Spender erfolgreich gespeichert (' + data.id + ').');
                $rootScope.back();
            })
            .catch(function (err) {
                $rootScope.faultHandler(err);
                $scope.success = false;
            })
            .finally(function () {
                $log.debug('Finished');
            });
    };

}]);

donators.controller('DeleteDonatorCtrl', ['$scope', '$rootScope', '$routeParams', '$log', 'DonatorService', function ($scope, $rootScope, $routeParams, $log, DonatorService) {
    $log.debug('Deleting');
    $scope.success = false;
    $scope.id = $routeParams['id'];


    $scope.delete = function (id) {
        $log.debug('Deleting donator [' + id + ']');
        DonatorService.remove({id: id}).$promise
            .then(function (data) {
                $log.debug('Successful ' + data);
                $scope.success = true;
                $rootScope.successHandler('Spender wurde gelöscht.');
                $rootScope.back();
            })
            .catch(function (err) {
                $rootScope.faultHandler(err);
                $scope.success = false;
            })
            .finally(function () {
                $log.debug('Finished');
            });
    };
}]);

donators.controller('ListDonatorsCtrl', ['$scope', '$rootScope', '$log', '$location', '$route', 'DonatorService', function ($scope, $rootScope, $log, $location, $route, DonatorService) {
    $scope.route = $route;
    $scope.maxCount = 20;

    $scope.donators = [];

    $scope.showPage = function (page) {
        DonatorService.query({
            page: (page < 0) ? 0 : page,
            count: 15,
            sort: 'lastname',
            order: 'ASC'
        }).$promise
            .then(function (data) {
                $scope.donators = data.content;
                $scope.totalPages = data.totalPages;
                $scope.currentPage = data.number;
                $scope.isFirst = data.first;
                $scope.isLast = data.last;
            })
            .catch(function (err) {
                $rootScope.faultHandler(err);
            });
    };

    $scope.showPage(0);

    $scope.detail = function (id) {
        $scope.selectedDonator = DonatorService.get({id: id});
        $('#detailDonatorDialog').modal('show');
    };

    $scope.edit = function (id) {
        var dlg = $('#detailDonatorDialog');
        dlg.on('hidden.bs.modal', function () {
            $scope.$apply(function () {
                $location.path('/edit-donator/' + id);
            });
        });
        dlg.modal('hide');
    };

    $scope.delete = function (id) {
        var dlg = $('#detailDonatorDialog');
        dlg.on('hidden.bs.modal', function () {
            $scope.$apply(function () {
                $location.path('/delete-donator/' + id);
            });
        });
        dlg.modal('hide');
    };

    $scope.showDonations = function (id) {
        var dlg = $('#detailDonatorDialog');
        dlg.on('hidden.bs.modal', function () {
            $scope.$apply(function () {
                $location.path('/list-donations/donator/' + id);
            });
        });
        dlg.modal('hide');
    }
}]);