'use strict';

var app = angular.module('spenDB');

app.config(['$routeProvider', function ($routeProvider) {
    return $routeProvider
        .when('/shutdown', {
            templateUrl: 'components/shutdown/shutdown.html',
            controller: 'ShutdownCtrl'
        });
}]);

app.controller('ShutdownGlobalCtrl', ['$scope', '$location', '$log', function ($scope, $location, $log) {
    $scope.shutdown = function () {
        $log.debug('Global shutdown forwarding to shutdown page');
        $location.path('/shutdown');
    }
}]);

/*
 * Shutdown ctrl.
 */
app.controller('ShutdownCtrl', ['$scope', '$log', '$resource', '$location', '$interval', function ($scope, $log, $resource, $location, $interval) {
    $scope.shutdownInProgress = false;
    $scope.up = true;
    $scope.progressCounter = -10;

    var shutdownTask;

    function shutdown() {
        $log.debug("Shutting down system!");
        $scope.shutdownInProgress = true;

        $resource('/shutdown', {}, {shutdown: {method: 'POST'}}).shutdown();


        shutdownTask = $interval(function () {
            $log.debug('Shutting down');
            $scope.shutdownInProgress = true;
            $resource('/health', {}, {health: {method: 'GET'}}).health().$promise
                .then(function (data) {
                    $log.debug(data);
                    $scope.progressCounter = (($scope.progressCounter + 10) % 100) + 10;
                    $log.debug('Tick is [' + $scope.progressCounter + ']');

                })
                .catch(function () {
                    stop();
                    $scope.progressCounter = 100;
                    $scope.shutdownInProgress = false;
                    $scope.up = false;
                    $log.debug('Shutdown finished');
                });
        }, 250);
    }

    function stop() {
        $interval.cancel(shutdownTask);
    }

    shutdown();
}]);