'use strict';

var backup = angular.module('spenDB.backup', [
    'ngRoute'
]);

backup.config(['$routeProvider', function ($routeProvider) {
    return $routeProvider
        .when('/import', {
            templateUrl: 'components/backup/import.html',
            controller: 'BackupImportController'
        })
        .when('/export', {
            templateUrl: 'components/backup/export.html',
            controller: 'BackupExportController'
        });
}]);

backup.service('BackupService', ['$resource', function ($resource) {
    return $resource('/services/:op', {}, {
        import: {
            method: 'POST',
            params: {
                op: 'import'
            },
            isArray: false
        }
    });
}]);

donatorList.controller('BackupImportController', ['$scope', '$rootScope', '$log', 'BackupService', function ($scope, $rootScope, $log, BackupService) {
    $scope.load = function (file) {
        $log.debug('Loading backup file...');
        $log.debug('file contents [' + file + ']');
        BackupService.import(file).$promise
            .then(function (data) {
                $log.debug('import successful');
                $rootScope.successHandler('Backup wurde erfolgreich importiert. Es wurden ' + data.donators + ' Spender und ' + data.donations + ' Spenden hinzugefügt.');
            })
            .catch($rootScope.faultHandler);
    }
}]);

donatorList.controller('BackupExportController', ['$scope', '$rootScope', '$filter', function ($scope, $rootScope, $filter) {
    $scope.backupFileName = 'spendb_backup_' + $filter('date')(Date.now(), 'yyyyMMddhhmm') + '.json';
    $scope.click = function () {
        $rootScope.successHandler('Das Backup wird automatisch unter dem Dateinamen ' + $scope.backupFileName + ' gespeichert.');
    };
}]);