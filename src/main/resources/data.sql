-- initialize T_PERSON
-- initialize T_DONATION
-- initialize T_HOUSEWAREITEMS
MERGE INTO T_HOUSEWARESITEM USING
  (VALUES (1, 'DISHES','Geschirr'), (2, 'CUTLERY', 'Besteck'), (3, 'GLASS', 'Gläser'),(4, 'KITCHEN_UTILS', 'Küchenutensilien'), (5, 'DUVET_COVER', 'Bettbezüge'), (6,'TOWEL','Handtücher'), (7,'CLEANING_POWDER','Putzmittel'))
    AS vals(ID, KEY, NAME)
ON T_HOUSEWARESITEM.KEY = vals.KEY AND T_HOUSEWARESITEM.ID = vals.ID
WHEN MATCHED THEN
UPDATE SET T_HOUSEWARESITEM.NAME=vals.NAME
WHEN NOT MATCHED THEN INSERT VALUES (vals.ID, vals.KEY, vals.NAME);
